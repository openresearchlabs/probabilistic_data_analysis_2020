# Zoom info

Join Zoom Meeting:

[https://utu.zoom.us/j/603598713](https://utu.zoom.us/j/603598713)

Meeting ID: 603 598 713

One tap mobile  
+358942451488,,603598713# Finland  
+358341092129,,603598713# Finland  

Dial by your location  
        +358 9 4245 1488 Finland  
        +358 3 4109 2129 Finland  
Meeting ID: 603 598 713  
Find your local number: [https://utu.zoom.us/u/cbQPTZa1mH](https://utu.zoom.us/u/cbQPTZa1mH)

Join by SIP  
603598713@109.105.112.236  
603598713@109.105.112.235

Join by H.323  
109.105.112.236  
109.105.112.235  
Meeting ID: 603 598 713