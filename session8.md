# Research seminar on probabilistic data analysis: Session 8 (June 26, 2020)

  * [Zoom link](zoom.md)
  * [Zulip chat](https://utu.zulipchat.com/#narrow/stream/230589-bda)
  
## Discussion topic: Hamiltonian Monte Carlo

 * Read [A Conceptual Introduction to HMC](https://arxiv.org/pdf/1701.02434.pdf) 
 * Read [Implementation of HMC in modeling a SIR model](https://www.sciencedirect.com/science/article/pii/S1755436519300325)

### Optional reading:

 * Skim through sections 12.4, 12.5, 12.6 (pages 300 to 308 BDA) and Appendix C.4 (pages 601 to 605 BDA)
 * Look at [No-U-Turn](http://www.stat.columbia.edu/~gelman/research/published/nuts.pdf)
 * Look at [slides](https://github.com/avehtari/BDA_course_Aalto/blob/master/slides/slides_ch12.pdf)
 * [Demo in R](https://avehtari.github.io/BDA_R_demos/demos_ch12/demo12_1.html)

### Nice intuitive presentations:

 * [Markov Chains: Why Walk When You Can Flow?](https://elevanth.org/blog/2017/11/28/build-a-better-markov-chain/)
 #### Videos:
 * [Aki Vehtari](https://aalto.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=1744f6a0-84d3-4218-8a86-aae600ba7e84)
 * [Michael Betancourt](https://www.youtube.com/watch?v=jUSZboSq1zg&list=WL&index=6&t=0s)
