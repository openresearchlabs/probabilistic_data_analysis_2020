data {
  int N; // number of groups
  int M; // samples size/group
  matrix[M, N] Y;
}

parameters {
  real mu;
  real<lower=0> sigma;
}

model {
  to_row_vector(Y) ~ normal(mu, sigma);
}

generated quantities {
  real Y_tilda = normal_rng(mu, sigma);
}