

## Functions **************** ####

# Binomial likelihood separately for both groups
likelihood <- function(par, data = c(674, 39)) {
  
  # Beta
  par^data[2]*(1 - par)^(data[1] - data[2])
  
}

# Beta(1, 1) prior (uniform)
prior <- function(par) {
  dbeta(par, 1, 1)
}

# Unnormalized posterior, p.35 in BDA3
posterior <- function(par, data) {
  dbeta(par, 1 + data[2], 1 + data[1] - data[2])
}

# Function to sample from posterior
sample_posterior <- function(n, hyper_pars = c(1, 1), data) {
  rbeta(n, hyper_pars[1] + data[2], hyper_pars[2] + data[1] - data[2])
}


## Posterior **************** ####

beta_df <- data.frame(
  p0 = sample_posterior(1000,
                        hyper_pars = c(1, 1), 
                        data = c(674, 39)), 
  p1 = sample_posterior(1000,
                        hyper_pars = c(1, 1),
                        data = c(680, 22))
)


# Odds ratio
beta_df <- beta_df %>% 
  mutate(odds_ratio = (p1/(1-p1))/(p0/(1-p0)))



## Plot ********************* ####

p_beta_odds_ratio <- beta_df %>% 
  ggplot(aes(x = odds_ratio)) + 
  geom_histogram(bins = 30, fill = "steelblue", color = "black") +
  geom_vline(aes(xintercept = mean(beta_df$odds_ratio)), color = "red") +
  geom_vline(xintercept = quantile(beta_df$odds_ratio, c(.025), color = "black")) +
  geom_vline(xintercept = quantile(beta_df$odds_ratio, c(.975), color = "black")) +
  labs(title = "Odds ratio", 
       subtitle = paste0("Mean = ",
                         mean(beta_df$odds_ratio) %>%
                           round(2), 
                         ", 95% interval = [",
                         quantile(beta_df$odds_ratio, c(.025)) %>%
                           round(2), 
                         ", ", 
                         quantile(beta_df$odds_ratio, c(.975)) %>%
                           round(2), 
                         "]"))


p_beta_odds_ratio

# Assuming an uninformative prior Beta(1, 1)
# there is evidence (probability mean(beta_df$odds_ratio > .5) = 0.659)
# that the odds ratio is above 0.5. 


